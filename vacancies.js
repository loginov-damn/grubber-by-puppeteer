let puppeteer = require('puppeteer');
let fs = require('fs');

(async () => {

  let vacanciesUrl = 'https://moikrug.ru/vacancies?divisions%5B%5D=backend&divisions%5B%5D=frontend&divisions%5B%5D=apps&divisions%5B%5D=software&divisions%5B%5D=testing';
  let loginUrl = 'https://moikrug.ru/users/sign_in';

  let browser = await puppeteer.launch({ headless: false });
  let page = await browser.newPage();

  // await page.goto(loginUrl, { waitUntil: 'networkidle2' });

  // await page.type('#user_email', 'rodbka93@gmail.com');
  // await page.type('#user_password', 'porsche92221');
  // await page.keyboard.press('Enter');

  // await sleep(1500);

  await page.goto(vacanciesUrl, { waitUntil: 'networkidle2' });

  let fileStream = fs.createWriteStream('./output.csv');

  for (let i = 0; i < 1000; i++) {

    let handles = await page.$$('.job');

    await Promise.all([handles.forEach(async handle => {
      let title = await handle.$eval('div.title', node => node.innerText);
      let skills = await handle.$$eval('.skill', nodes => nodes.map(n => n.innerText).join(','));

      let data = title + '\t' + skills;
      fileStream.write(data + '\n');
      console.log(data);
    })]);

    await sleep(500);

    await page.click('a[rel=next].page');

    await sleep(1000);

  }



  // await handles[0].evaluate((item) => {
  //   console.log(item.querySelector('a[target=_blank]').innerText);
  // });

  // handles.forEach(async handle => {
  //   console.log(await handle.$(".username").getProperty('textContent'));
  // });

  // const element = await page.$eval('.username', el => {
  //   console.log(el._blank);
  // });

  // let data = await page.evaluate(() => {
  //   let elements = document.getElementsByClassName('search-items');

  //   return elements;
  // })

  // console.log(data);
})();

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
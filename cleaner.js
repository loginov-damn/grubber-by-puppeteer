let fs = require('fs');

(async () => {

  console.log('Cleaning of file: ', process.argv[2]);
  
  let data;
  let readStream = fs.createReadStream('./' + process.argv[2]);
  
  readStream.on('open', () => {
    readStream.pipe(data);

    console.log(data);
    
  });

  readStream.on('error', err => {
    res.end(err);
  });
  
})();